package boreas.com.springbootservicioproducto.web;

import boreas.com.springbootservicioproducto.model.Producto;
import boreas.com.springbootservicioproducto.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @GetMapping("/listar")
    public List<Producto> list() {
        return productoService.findAll();
    }

    @GetMapping("/ver/{id}")
    public Producto detail(@PathVariable Long id) {
        return productoService.findById(id);
    }
}
