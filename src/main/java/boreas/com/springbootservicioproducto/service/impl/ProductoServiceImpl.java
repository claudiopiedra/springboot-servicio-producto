package boreas.com.springbootservicioproducto.service.impl;

import boreas.com.springbootservicioproducto.model.Producto;
import boreas.com.springbootservicioproducto.repository.ProductoDao;
import boreas.com.springbootservicioproducto.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductoServiceImpl implements ProductoService  {

    @Autowired
    private ProductoDao productoDao;

    @Override
    @Transactional(readOnly=true)
    public List<Producto> findAll() {
        return productoDao.findAll();
    }

    @Override
    @Transactional(readOnly=true)
    public Producto findById(Long id) {
        return productoDao.findById(id).orElse(null);
    }
}
