package boreas.com.springbootservicioproducto.service;

import boreas.com.springbootservicioproducto.model.Producto;

import java.util.List;

public interface ProductoService {

    List<Producto> findAll();
    Producto findById(Long id);
}